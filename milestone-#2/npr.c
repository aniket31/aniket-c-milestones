#include <stdio.h>
#include <conio.h>
int fact(int z);
void main()
{
	int n, r, npr;
	clrscr();
	printf("enter vals of n and r");
	scanf("%d%d", &n, &r);
	npr = fact(n) / fact(n - r);
	printf("ans=%d", npr);
	getch();
}
int fact(int z)
{
	int i;
	int f = 1;
	if (z == 0)
	{
		return (f);
	}
	else
	{
		for (i = 1; i <= z; i++)
		{
			f = f * i;
		}
	}
	return (f);
}