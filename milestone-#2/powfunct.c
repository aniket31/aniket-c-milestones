#include <stdio.h>
#include <conio.h>
int power(int base, int exponent)
{
    int result = 1;
    for (exponent; exponent > 0; exponent--)
    {
        result = result * base;
    }
    return result;
}
int main()
{
    int base, exponent;
    clrscr();
    printf("enter base");
    scanf("%d", &base);
    printf("enter exponent");
    scanf("%d", &exponent);
    int res = power(base, exponent);
    printf("answer=%d", res);
    getch();
    return 0;
}