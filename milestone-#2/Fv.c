#include<stdio.h>
#include<conio.h>
#include<math.h>

double FV(double rate, unsigned int periods, double PV ){
    int future_value = PV * pow(1+rate, periods );
    return future_value;
}

double main(){
    double rate;
    unsigned int periods;
    double PV;

    printf("Enter the rate: \n");
    scanf("%lf", &rate);

    printf("Enter the periods: \n");
    scanf("%d", &periods);

    printf("Enter the present Value: \n");
    scanf("%lf", &PV);

    double result = FV(rate, periods, PV);
    printf("Present Value is: %lf", result);
    getch();
    return 0;
}